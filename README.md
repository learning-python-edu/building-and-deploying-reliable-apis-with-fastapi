# Building and Deploying Reliable APIs with FastAPI

Sample code for "Building and Deploying Reliable APIs with FastAPI" video course from Safari Learning.

## Project setup

### Install dependencies and activate environment

Project requires Python 3.9 or newer. To start working with the project need to install some dependencies.

First need to install `pipenv` with the following command:
```shell
pip install pipenv
```

Next need to install dependencies declared in `Pipfile` including dev dependencies. Install it with the following command:
```shell
pipenv install --dev
```

Finally activate newly created environment:
```shell
pipenv shell
```

### Generate data model from OpenAPI description

Data model should be generated from `oas.yaml` file with the help of `datamodel-codegen` tool as following:
```shell
datamodel-codegen --input oas.yaml --output todo/schemas.py
```

## Run server

To run server execute following command:
```shell
uvicorn todo.server:server --reload
```

This will load server in reload mode, when it will reload on any file changes.

Sample endpoint can be accessed at the following URL: http://127.0.0.1:8000/todo.

## Access endpoints API

FastAPI automatically generates OpenAPI documentation, which can be accessed at the following URL:
http://127.0.0.1:8000/docs. 
